package com.conygre.demo.controller;


import com.conygre.demo.entities.Customer;
import com.conygre.demo.entities.Interactions;
import com.conygre.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    private CustomerService cs;

    @GetMapping
    List<Customer> getAll(){ return cs.getCustomers(); }

    @RequestMapping(method = RequestMethod.GET, value = "customer/{id}")
    public Customer getCustomerById(@PathVariable("id") int id) { return cs.getCustomerById(id); }

    @RequestMapping(method = RequestMethod.GET, value = "customer/active")
    public List<Customer> getActiveCustomers() { return cs.getActiveCustomers(); }

    @RequestMapping(method = RequestMethod.POST, value = "customer/add")
    public void addCustomer(@RequestBody Customer customer) { cs.addNewCustomer(customer); }

    @RequestMapping(method = RequestMethod.PUT, value = "customer/delete/{id}")
    public Customer softDeleteCustomerById(@PathVariable("id") int id) { return cs.softDeleteCustomerById(id); }

    @RequestMapping(method = RequestMethod.PUT, value = "customer/update/{id}")
    public Customer updateCustomerById(@PathVariable("id") int id, @RequestBody Customer customer) { return cs.updateCustomerById(id, customer); }

    @RequestMapping(method = RequestMethod.GET, value = "customer/interactions/{id}")
    public List<Interactions> getCustomerInteractionsById(@PathVariable("id") int id) { return cs.getCustomerInteractionById(id); }

    @RequestMapping(method = RequestMethod.GET, value = "interaction/all")
    public List<Interactions> getAllInteractions() { return cs.getAllInteractions(); }

    @RequestMapping(method = RequestMethod.GET, value = "interaction/{id}")
    public Interactions getInteractionById(@PathVariable("id") int id) { return cs.getInteractionById(id); }

    @RequestMapping(method = RequestMethod.PUT, value = "/interaction/update/{id}")
    public Interactions updateCustomerInteractionsById(@PathVariable("id") int id, @RequestBody Interactions interaction) { return cs.updateCustomerInteractionById(id, interaction); }

    @RequestMapping(method = RequestMethod.POST, value = "/interaction/add")
    public Interactions addCustomerInteraction(@RequestBody Interactions interaction) { return cs.addCustomerInteraction(interaction); }
}
