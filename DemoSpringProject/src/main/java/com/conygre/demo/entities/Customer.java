package com.conygre.demo.entities;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="first_name") private String first_name;
    @Column(name="last_name") private String last_name;
    @Column(name="city") private String city;
    @Column(name="zipcode") private Integer zipcode;
    @Column(name="date_registered") private Date date_registered;
    @Column(name="is_active") private Boolean is_active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public Date getDate_registered() {
        return date_registered;
    }

    public void setDate_registered(Date date_registered) {
        this.date_registered = date_registered;
    }

    public boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    @OneToMany(mappedBy = "customer", cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<Interactions> interactionsList = new ArrayList<Interactions>();

    public List<Interactions> getInteractionsList() { return interactionsList; }

    public void setInteractionsList(List<Interactions> interactionsList) { this.interactionsList = interactionsList; }
}
