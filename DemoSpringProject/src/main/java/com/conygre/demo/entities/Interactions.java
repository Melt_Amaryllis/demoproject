package com.conygre.demo.entities;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "interactions")
public class Interactions implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "customer_id") private Integer individual_customer_id;
    @Column(name = "date_of_interaction") private Date date_of_interaction;
    @Column(name = "description_of_interaction") private String description_of_interaction;
    @Column(name = "method_of_interaction") private String method_of_interaction;
    @Column(name = "issue_resolved") private Boolean issue_resolved;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getIndividual_customer_id() {
        return individual_customer_id;
    }

    public void setIndividual_customer_id(Integer individual_customer_id) {
        this.individual_customer_id = individual_customer_id;
    }


    public Date getDate_of_interaction() {
        return date_of_interaction;
    }

    public void setDate_of_interaction(Date date_of_interaction) {
        this.date_of_interaction = date_of_interaction;
    }

    public String getDescription_of_interaction() {
        return description_of_interaction;
    }

    public void setDescription_of_interaction(String description_of_interaction) {
        this.description_of_interaction = description_of_interaction;
    }

    public String getMethod_of_interaction() {
        return method_of_interaction;
    }

    public void setMethod_of_interaction(String method_of_interaction) {
        this.method_of_interaction = method_of_interaction;
    }

    public Boolean getIssue_resolved() {
        return issue_resolved;
    }

    public void setIssue_resolved(Boolean issue_resolved) {
        this.issue_resolved = issue_resolved;
    }

    @JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne
    private Customer customer;
}
