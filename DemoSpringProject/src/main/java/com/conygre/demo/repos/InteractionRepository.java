package com.conygre.demo.repos;

import com.conygre.demo.entities.Interactions;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InteractionRepository extends JpaRepository<Interactions, Integer> {
}
