package com.conygre.demo.services;

import com.conygre.demo.entities.Customer;
import com.conygre.demo.entities.Interactions;
import com.conygre.demo.repos.CustomerRepo;
import com.conygre.demo.repos.InteractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{
    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private InteractionRepository interactionRepository;


    public List<Customer> getCustomers() {
        return customerRepo.findAll();
    }

    public Customer getCustomerById(int id) {
        return customerRepo.findById(id).get();
    }

    public Customer addNewCustomer(Customer customer) {
        return customerRepo.save(customer);
    }

    public Customer softDeleteCustomerById(int id) {
        Customer customerToDelete = customerRepo.findById(id).get();
        customerToDelete.setIs_active(false);
        return customerRepo.save(customerToDelete);
    }

    public Customer updateCustomerById(int id, Customer customer) {
        Customer customerToUpdate = customerRepo.findById(id).get();
        customerToUpdate.setFirst_name(customer.getFirst_name());
        customerToUpdate.setLast_name(customer.getLast_name());
        customerToUpdate.setCity(customer.getCity());
        customerToUpdate.setZipcode(customer.getZipcode());
        customerToUpdate.setDate_registered(customer.getDate_registered());
        customerToUpdate.setIs_active(customer.getIs_active());
        return customerRepo.save(customerToUpdate);
    }

    public List<Interactions> getCustomerInteractionById(int id) {
        return customerRepo.findById(id).get().getInteractionsList();
    }

    public Interactions getInteractionById(int id) {
        return interactionRepository.findById(id).get();
    }

    public Interactions updateCustomerInteractionById(int id, Interactions interaction) {
        return interactionRepository.save(interaction);
    }

    public Interactions addCustomerInteraction(Interactions interaction) {
        return interactionRepository.save(interaction);
    }

    public List<Customer> getActiveCustomers() {
        return customerRepo.findByActive();
    }

    public List<Interactions> getAllInteractions() {
        return interactionRepository.findAll();
    }
}
