package com.conygre.demo.services;

import com.conygre.demo.entities.Customer;
import com.conygre.demo.entities.Interactions;

import java.util.List;

public interface CustomerService {
    List<Customer> getCustomers();

    Customer getCustomerById(int id);

    Customer addNewCustomer(Customer customer);

    Customer softDeleteCustomerById(int id);

    Customer updateCustomerById(int id, Customer customer);

    Interactions updateCustomerInteractionById(int id, Interactions interaction);

    List<Interactions> getCustomerInteractionById(int id);

    Interactions addCustomerInteraction(Interactions interaction);

    Interactions getInteractionById(int id);

    List<Customer> getActiveCustomers();

    List<Interactions> getAllInteractions();
}
