CREATE TABLE `customer` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `first_name` varchar(45) NOT NULL,
                            `last_name` varchar(45) NOT NULL,
                            `city` varchar(45) NOT NULL,
                            `zipcode` int NOT NULL,
                            `date_registered` varchar(45) NOT NULL,
                            `is_active` tinyint NOT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `interactions` (
                                `id` int NOT NULL AUTO_INCREMENT,
                                `customer_id` int NOT NULL,
                                `date_of_interaction` varchar(45) DEFAULT NULL,
                                `description_of_interaction` varchar(45) DEFAULT NULL,
                                `method_of_interaction` varchar(45) DEFAULT NULL,
                                `issue_resolved` tinyint DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
